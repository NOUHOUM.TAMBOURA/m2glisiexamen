package com.isi.patientservice;

import com.isi.patientservice.dao.PatientRepository;
import com.isi.patientservice.entite.Patient;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.web.bind.annotation.CrossOrigin;


import java.util.Date;

@SpringBootApplication
public class PatientServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatientServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(PatientRepository patientRepository, RepositoryRestConfiguration restConfiguration){
		return  args -> {
			restConfiguration.exposeIdsFor(Patient.class);
//			restConfiguration.getCorsRegistry()
//					.addMapping("/**") // autoriser tout les methodes apres /
//					.allowedOrigins("*") // quelque soit de domaine
//					.allowedHeaders("*") // quelque soit entete
	//				.allowedMethods("OPTIONS","HEAD","GET","PUT","POST","DELETE","PATCH");
			for (int i=0; i<20; i++) {
				patientRepository.save(new Patient("TAMBOURA", "NOUHOUM", "DAKAR", "87 654 78 54", new Date(), "M"));
				patientRepository.save(new Patient("DIARRA", "OUMAR", "DAKAR", "87 666 78 54", new Date(), "M"));
				patientRepository.save(new Patient("COULIBALY", "OUMOU", "DAKAR", "87 876 78 54", new Date(), "F"));
				patientRepository.save(new Patient("TRAORE", "ALIOU", "DAKAR", "87 111 78 54", new Date(), "M"));
				patientRepository.save(new Patient("CISSE", "ABDOU", "DAKAR", "87 444 78 54", new Date(), "M"));
			}
			};
	}

//	@Bean
//	public CorsFilter corsFilter() {
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//		CorsConfiguration config = new CorsConfiguration();
//		config.setAllowCredentials(true);
//		config.addAllowedOrigin("*");
//		config.addAllowedHeader("*");
//		config.addAllowedMethod("OPTIONS");
//		config.addAllowedMethod("GET");
//		config.addAllowedMethod("POST");
//		config.addAllowedMethod("PUT");
//		config.addAllowedMethod("DELETE");
//		source.registerCorsConfiguration("/**", config);
//		return new CorsFilter();
//	}
}
