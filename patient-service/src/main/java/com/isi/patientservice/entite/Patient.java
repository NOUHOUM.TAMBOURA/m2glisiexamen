package com.isi.patientservice.entite;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idpatient;
    @Column(length = 50)
    private String nompatient;
    @Column(length = 50)
    private String prenompatient;
    @Column(length = 50)
    private String adressepatient;
    @Column(length = 50)
    private String telpatient;
    @Column(length = 50)
    private Date dateNaisspatient;
    @Column(length = 50)
    private String sexepatient;

    public Patient() {
    }

    public Patient(String nompatient, String prenompatient, String adressepatient, String telpatient, Date dateNaisspatient, String sexepatient) {
        this.nompatient = nompatient;
        this.prenompatient = prenompatient;
        this.adressepatient = adressepatient;
        this.telpatient = telpatient;
        this.dateNaisspatient = dateNaisspatient;
        this.sexepatient = sexepatient;
    }

    public long getIdpatient() {
        return idpatient;
    }

    public void setIdpatient(long idpatient) {
        this.idpatient = idpatient;
    }

    public String getNompatient() {
        return nompatient;
    }

    public void setNompatient(String nompatient) {
        this.nompatient = nompatient;
    }

    public String getPrenompatient() {
        return prenompatient;
    }

    public void setPrenompatient(String prenompatient) {
        this.prenompatient = prenompatient;
    }

    public String getAdressepatient() {
        return adressepatient;
    }

    public void setAdressepatient(String adressepatient) {
        this.adressepatient = adressepatient;
    }

    public String getTelpatient() {
        return telpatient;
    }

    public void setTelpatient(String telpatient) {
        this.telpatient = telpatient;
    }

    public Date getDateNaisspatient() {
        return dateNaisspatient;
    }

    public void setDateNaisspatient(Date dateNaisspatient) {
        this.dateNaisspatient = dateNaisspatient;
    }

    public String getSexepatient() {
        return sexepatient;
    }

    public void setSexepatient(String sexepatient) {
        this.sexepatient = sexepatient;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "idpatient=" + idpatient +
                ", nompatient='" + nompatient + '\'' +
                ", prenompatient='" + prenompatient + '\'' +
                ", adressepatient='" + adressepatient + '\'' +
                ", telpatient='" + telpatient + '\'' +
                ", dateNaisspatient=" + dateNaisspatient +
                ", sexepatient='" + sexepatient + '\'' +
                '}';
    }
}
