package com.isi.rendezvousservice;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.isi.rendezvousservice.Service.DetailsRendezVous;
import com.isi.rendezvousservice.dao.MedecinService;
import com.isi.rendezvousservice.dao.PatientService;
import com.isi.rendezvousservice.dao.RendezVousRepository;
import com.isi.rendezvousservice.entite.Medecin;
import com.isi.rendezvousservice.entite.Patient;
import com.isi.rendezvousservice.entite.RendezVous;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;

@SpringBootApplication
//pour aciver OpenFeignClients
@EnableFeignClients
public class RendezvousServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RendezvousServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(RendezVousRepository rendezVousRepository,
							 RepositoryRestConfiguration repositoryRestConfiguration ,
							 PatientService patientService , MedecinService medecinService,
							 DetailsRendezVous detailsRendezVous){
		return  args -> {
			repositoryRestConfiguration.exposeIdsFor(RendezVous.class);
			System.out.println("============================Liste des Patients===========================================");
			// affiche la liste de tout les patients
			PagedModel<Patient> patients = patientService.findAllPatient();
			PagedModel<Medecin> medecins = medecinService.findAllMedecin();
			 patients.getContent().forEach(p ->{
				medecins.getContent().forEach(m ->{
					rendezVousRepository.save(new RendezVous(new Date(),p.getIdpatient(),m.getIdmedecin()));
//				System.out.println(p.getAdressepatient());
//			    System.out.println(p.getDateNaisspatient());
//			    System.out.println(p.getNompatient());
//			    System.out.println(p.getPrenompatient());
//			    System.out.println(p.getTelpatient());
//			    System.out.println(p.getIdpatient());
//			    System.out.println("===========");
//			    System.out.println(m.getIdmedecin());
//				System.out.println(m.getAdressemedecin());
//		        System.out.println(m.getDateNaissmedecin());
//			    System.out.println(m.getPrenommedecin());
//			    System.out.println(m.getTelmedecin());

			});

			});
//
//			Patient patient1=patientService.findPatientById(2L);
//			System.out.println("++++++++++++++++PATIENT SERVICE++++++++++++++++++");
//			System.out.println(patient1.getAdressepatient());
//			System.out.println(patient1.getDateNaisspatient());
//			System.out.println(patient1.getNompatient());
//			System.out.println(patient1.getPrenompatient());
//			System.out.println(patient1.getTelpatient());
//			System.out.println("+++++++++++++++PATIENT SERVICE++++++++++++++++++++");
//			Medecin medecin1 = medecinService.findMedecinById(1L);
//			System.out.println(medecin1.getAdressemedecin());
//			System.out.println(medecin1.getDateNaissmedecin());
//			System.out.println(medecin1.getIdmedecin());
//			System.out.println(medecin1.getPrenommedecin());
//			System.out.println(medecin1.getTelmedecin());
//			System.out.println("+++++++++++++++++++++DETAILS+++++++++++++++++++++++++++++");
//			System.out.println(detailsRendezVous.getRendezVous(1L));
//			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
//



		};
	}
}
