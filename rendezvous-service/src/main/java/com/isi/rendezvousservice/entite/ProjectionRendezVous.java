package com.isi.rendezvousservice.entite;

import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name = "projectionRendezVous", types = RendezVous.class)
public interface ProjectionRendezVous {
    public Long getidrendezvous();
    public Date getdaterendezvous();
    public Patient getpatient();
    public Medecin getmedecin();
}
