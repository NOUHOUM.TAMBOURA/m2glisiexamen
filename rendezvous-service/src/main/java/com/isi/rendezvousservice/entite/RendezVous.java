package com.isi.rendezvousservice.entite;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
public class RendezVous {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idrendezvous;
    private Date daterendezvous;
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long idpatient;
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long idmedecin;

    //pour afficher des informations sur le patient et medecins dans /rendezVouses api
    @Transient // pour lui dire de ne pas enregistrer attributs dans la BD
    private Patient patient;
    @Transient
    private Medecin medecin;

    public RendezVous() {
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Medecin getMedecin() {
        return medecin;
    }

    public void setMedecin(Medecin medecin) {
        this.medecin = medecin;
    }

    public RendezVous(Date daterendezvous, Long idpatient, Long idmedecin, Patient patient, Medecin medecin) {
        this.daterendezvous = daterendezvous;
        this.idpatient = idpatient;
        this.idmedecin = idmedecin;
        this.patient = patient;
        this.medecin = medecin;
    }

    public RendezVous(Date daterendezvous, Long idpatient, Long idmedecin) {
        this.daterendezvous = daterendezvous;
        this.idpatient = idpatient;
        this.idmedecin = idmedecin;
    }

    public long getIdrendezvous() {
        return idrendezvous;
    }

    public void setIdrendezvous(long idrendezvous) {
        this.idrendezvous = idrendezvous;
    }

    public Date getDaterendezvous() {
        return daterendezvous;
    }

    public void setDaterendezvous(Date daterendezvous) {
        this.daterendezvous = daterendezvous;
    }

    public Long getIdpatient() {
        return idpatient;
    }

    public void setIdpatient(Long idpatient) {
        this.idpatient = idpatient;
    }

    public Long getIdmedecin() {
        return idmedecin;
    }

    public void setIdmedecin(Long idmedecin) {
        this.idmedecin = idmedecin;
    }

    @Override
    public String toString() {
        return "RendezVous{" +
                "idrendezvous=" + idrendezvous +
                ", daterendezvous=" + daterendezvous +
                ", idpatient=" + idpatient +
                ", idmedecin=" + idmedecin +
                '}';
    }
}
