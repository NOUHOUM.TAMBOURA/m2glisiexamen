package com.isi.consultationservice.dao;

import com.isi.consultationservice.entite.RendezVous;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "RENDEZ-VOUS-API")
public interface RendezVousService {
    @GetMapping("/rendezVouses/{id}")
    public RendezVous findRendezVousById(@PathVariable(name = "id") Long id);

    @GetMapping("/rendezVouses")
    public PagedModel<RendezVous> findAllRendezVous();
}
