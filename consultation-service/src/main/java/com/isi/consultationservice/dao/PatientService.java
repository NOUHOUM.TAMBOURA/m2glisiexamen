package com.isi.consultationservice.dao;

import com.isi.consultationservice.entite.Patient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "PATIENT-SERVICE")
public interface PatientService {
    @GetMapping("/patients/{id}")
    public Patient findPatientById(@PathVariable(name = "id") Long id);

    @GetMapping("/patients")
    public PagedModel<Patient> findAllPatient();


}
