package com.isi.medecinservice.dao;

import com.isi.medecinservice.entite.Medecin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface MedecinRepository extends JpaRepository<Medecin, Long> {

    // chercher un medecin par nom
    @RestResource(path = "/cherchernom")
    public Page<Medecin> findByNommedecinContains(@Param("nom") String nom , Pageable pageable);
}
